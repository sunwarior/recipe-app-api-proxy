# Recipe App API proxy

NGINX proxy app for our recipe app API.

## Usage

### ENV Variables

- `LISTEN_PORT` - The listening port (default: `8000`)
- `APP_HOST` - The app host (default: `app`)
- `APP_PORT` - The app port forward (default: `9000`)
